from django.conf.urls import url

import views

urlpatterns = [
    url(r'^enter', views.enter, name='enter'),
    url(r'^logout', views.logout, name='logout'),
    url(r'^register', views.register, name='reg'),
]
