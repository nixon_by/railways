from django.shortcuts import render, redirect
from django.contrib import auth

import forms


def enter(request):
    if request.POST:
        EnterForm = forms.EnterFrom(request.POST)
        if EnterForm.is_valid():
            login = EnterForm.cleaned_data['login']
            password = EnterForm.cleaned_data['password']
            user = auth.authenticate(username=login, password=password)
            if user is not None:
                auth.login(request, user)
                return redirect('/freight')
            else:
                err_msg = 'Invalid login or password'
                return render(request, 'login/enter.html',
                              {'form': EnterForm,
                               'error': err_msg})
    else:
        EnterForm = forms.EnterFrom()
    return render(request, 'login/enter.html', {'form': EnterForm})


def logout(request):
    auth.logout(request)
    return redirect('/')


def register(request):
    pass
