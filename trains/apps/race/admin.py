from django.contrib import admin

import models


@admin.register(models.Race)
class RaceAdmin(admin.ModelAdmin):
    model = models.Race
    list_display = ['__unicode__', 'wagons']
    filter_vertical = ['cargos']
