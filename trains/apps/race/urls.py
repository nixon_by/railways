from django.conf.urls import url

import views

urlpatterns = [
    url(r'^delete/(?P<race_id>\d+)', views.delete, name='delete'),
    url(r'^add', views.add, name='add'),
    url(r'^(?P<race_id>\d+)', views.single, name='single'),
    url(r'^$', views.index, name='index'),
]
