# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('station', '0001_initial'),
        ('cargo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Race',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('wagons', models.IntegerField()),
                ('cargos', models.ManyToManyField(to='cargo.Cargo')),
                ('id_from', models.ForeignKey(related_name='race_from', to='station.Station')),
                ('id_to', models.ForeignKey(related_name='race_to', to='station.Station')),
            ],
        ),
    ]
