$(document).ready(function() {
        var tr = $('.data-table-body').children('tr');
        var color = tr.css('background-color');
        tr.on('mouseover', function () {
            $(this).css({'background-color': '#87CEFA'});
        });
        tr.on('mouseout', function () {
            $(this).css({'background-color': color});
        });
});
