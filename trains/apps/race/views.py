from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth.decorators import login_required

import models


@login_required(login_url='/auth/enter')
def index(request):
    user = auth.get_user(request).username
    races = models.Race.objects.all()
    return render(request, 'race/freight.html',
                  {'username': user,
                   'races': races})


@login_required(login_url='/auth/enter')
def delete(request, race_id):
    # user = auth.get_user(request).username
    models.Race.objects.filter(id=race_id).delete()
    return redirect('/races')


@login_required(login_url='/auth/enter')
def single(request, race_id):
    user = auth.get_user(request).username
    race = models.Race.objects.get(pk=race_id)
    return render(request, 'race/single.html',
                  {'username': user,
                   'race': race})


@login_required(login_url='/auth/enter')
def add(request):
    pass
