from django.db import models

from trains.apps.cargo.models import Cargo
from trains.apps.station.models import Station


class Race(models.Model):
    id_from = models.ForeignKey(Station, null=False, related_name='race_from')
    id_to = models.ForeignKey(Station, null=False, related_name='race_to')
    wagons = models.IntegerField(null=False)
    cargos = models.ManyToManyField(Cargo)

    def __unicode__(self):
        return "%s - %s" % (self.id_from.name, self.id_to.name)

    class Meta:
        ordering = ['id']
