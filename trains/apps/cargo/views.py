from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth.decorators import login_required

import models


# @login_required(login_url='/auth/enter')
def index(request):
    user = auth.get_user(request).username
    cargos = models.Cargo.objects.all()
    return render(request, 'cargo/cargos.html',
                  {'cargos': cargos,
                   'username': user})


@login_required(login_url='/auth/enter')
def delete(request, cargo_id):
    models.Cargo.objects.filter(id=cargo_id).delete()
    return redirect('/cargos')


@login_required(login_url='/auth/enter')
def add(request):
    pass
