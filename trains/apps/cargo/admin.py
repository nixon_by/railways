from django.contrib import admin

import models


@admin.register(models.Cargo)
class CargoAdmin(admin.ModelAdmin):
    model = models.Cargo
