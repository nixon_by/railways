from django.conf.urls import url

import views

urlpatterns = [
    url(r'^delete/(?P<cargo_id>\d+)', views.delete, name='delete'),
    url(r'^add', views.add, name='add'),
    url(r'^', views.index, name='index'),
]
