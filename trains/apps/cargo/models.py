from django.db import models


class Cargo(models.Model):
    name = models.CharField(max_length=150, null=False)

    def __unicode__(self):
        return "%s" % (self.name, )
