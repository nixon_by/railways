from django.shortcuts import render
from django.contrib import auth


def index(request):
    user = auth.get_user(request).username
    return render(request, 'main/index.html', {'username': user})


def about(request):
    user = auth.get_user(request).username
    return render(request, 'main/about.html', {'username': user})


def contact(request):
    user = auth.get_user(request).username
    return render(request, 'main/contact.html', {'username': user})


def services(request):
    user = auth.get_user(request).username
    return render(request, 'main/services.html', {'username': user})


def tourism(request):
    user = auth.get_user(request).username
    return render(request, 'main/tourism.html', {'username': user})


def corporate(request):
    user = auth.get_user(request).username
    return render(request, 'main/corporate.html', {'username': user})
