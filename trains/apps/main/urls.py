from django.conf.urls import include, url

import views

urlpatterns = [
    url(r'^about', views.about, name='about'),
    url(r'^contact', views.contact, name='contact'),
    url(r'^services', views.services, name='services'),
    url(r'^tourism', views.tourism, name='tourism'),
    url(r'^corporate', views.corporate, name='corporate'),
    url(r'^$', views.index, name='index')
]
