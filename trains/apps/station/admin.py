from django.contrib import admin

import models


@admin.register(models.Station)
class StationAdmin(admin.ModelAdmin):
    model = models.Station
