from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth.decorators import login_required

import models


# @login_required(login_url='/auth/enter')
def index(request):
    user = auth.get_user(request).username
    stations = models.Station.objects.all()
    return render(request, 'station/stations.html',
                  {'stations': stations,
                   'username': user})


@login_required(login_url='/auth/enter')
def delete(request, station_id):
    models.Station.objects.filter(id=station_id).delete()
    return redirect('/stations')


@login_required(login_url='/auth/enter')
def add(request):
    pass
