import os
ENV = os.environ.get('ENV', 'DEV')

DEV = ENV == 'DEV'
PROD = ENV == 'PROD'

if DEV:
    from .dev import *
if PROD:
    from .prod import *
